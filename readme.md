## Setup the project

For setup the project you need to make the next steps:

0) rename .env.example to .env
1) add you configuration data to .env.
2) add your pusher public key to js pet-detail.js:21

made the next commands in terminal

3) composer install
4) php artisan migrate
5) php artisan db:seed
6) npm install
7) npm run prod
8) crontab -e (and put the next string "* * * * * php /full/path/to/artisan schedule:run >> /dev/null 2>&1")

