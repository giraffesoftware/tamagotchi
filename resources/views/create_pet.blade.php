@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create pet</div>

                <div class="panel-body pets-list" >
                @foreach ($pets as $pet)
                <a href="{{ route('set-pet', ['user_pet_id'=>$pet['id']]) }}"><img src="/img/{{ $pet['name'] }}-image.jpg"></a>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .pets-list img {
        max-width:150px;
        cursor:pointer;
    }
</style>


@endsection

