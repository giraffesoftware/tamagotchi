@extends('layouts.app')

@section('content')

<div id="root">
    <pet-detail v-if="pet" :pet="pet" :premature-recharge="prematureRecharge"></pet-detail>
</div>
<script src="{{ mix('js/pet-detail.js') }}"></script>
@endsection