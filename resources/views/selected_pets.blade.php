@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Select pet</div>
                <div class="panel-body  pets-list">
                @if ($pets)
                    @foreach ($pets as $pet)
                        @if ($pet['alive'] == 1)
                            <a href="{{ route('pet', ['user_pet_id'=>$pet['user_pet_id']]) }}">
                                <img src="/img/{{ $pet['name'] }}-image.jpg" >
                            </a>
                        @else
                            <a href="#" class="died-pet">
                                <img src="/img/{{ $pet['name'] }}-image.jpg" >
                            </a>
                        @endif
                    @endforeach
                @else
                <p>Create you first pet <a href="{{ route('create-pet') }}"> here</a></p>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection