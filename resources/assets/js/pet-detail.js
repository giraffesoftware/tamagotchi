/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import api from './api';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo'

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '',
    cluster: 'eu',
    encrypted: true
});

window.Vue = require('vue');
var VueCookie = require('vue-cookie');
Vue.use(VueCookie);

Vue.component('pet-detail', require('./components/PetDetail.vue'));

const app = new Vue({
    el: '#root',
    data:{
        pet: null,
        statToNumber: {},
        prematureRecharge: null,
    },
    methods:{
    },
    mounted(){
        api.getPet().then(res=>{
            var pet ={};
            pet.id = window.location.href.split('/').pop();
            res.data[0].map(el=>{
                res.data[1].forEach(stat=>{
                    if(el.need_id == stat.id){
                        this.statToNumber[stat.name] = stat.id;
                        pet[stat.name] = el;
                    }
                });
            });
            res.data[2].forEach(el=>{
                if (el.id == res.data[0][0].user_pet.pet_id){
                    pet.type = el.name;
                }
            });
            this.pet = pet;
        });
        window.Echo.channel('pet-status-' + this.$cookie.get('user_id'))
            .listen('DecreaseNeedsStatusEvent', (e) => {
                if (this.pet.id == e.userPetId) {
                    for(var key in this.statToNumber) {
                        if (this.statToNumber[key] == e.needId) {
                            this.pet[key].value = e.value;
                        }
                    }
                }
            });
        window.Echo.channel('pet-status-' + this.$cookie.get('user_id'))
            .listen('IncreaseNeedsStatusEvent', (e) => {
                if (this.pet.id == e.userPetId) {
                    if (e.isUpdated) {
                        for(var key in this.statToNumber) {
                            if (this.statToNumber[key] == e.needId) {
                                this.pet[key].value = e.value;
                            }
                        }
                    } else {
                        this.prematureRecharge = e;
                    }
                }
            });
        window.Echo.channel('pet-status-' + this.$cookie.get('user_id'))
            .listen('PetDiedEvent', (e) => {
                if (this.pet.id == e.userPetId) {
                    location.href = '/';
                }
            });
    },

});
