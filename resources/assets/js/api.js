import axios from 'axios';

export default {
    getPet:(id)=>{
        return axios.post(`/selected-pet-info`,{
            user_pet_id:window.location.href.split('/').pop()
        });
    },
    updatePet:(obj)=>{
        return axios.post(`/increase-interval`,obj);
    },

}