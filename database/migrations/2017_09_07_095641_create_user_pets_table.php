<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_pets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('pet_id');
            $table->boolean('alive');
            $table->timestamps();
        });

        Schema::table('user_pets', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('pet_id')->references('id')->on('pets');
            $table->unique(['user_id', 'pet_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_pets', function (Blueprint $table) {
            $table->dropForeign('user_pets_user_id_foreign');
            $table->dropForeign('user_pets_pet_id_foreign');
            $table->dropUnique('user_pets_user_id_pet_id_unique');
        });

        Schema::dropIfExists('user_pets');
    }
}
