<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetNeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_needs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_pets_id');
            $table->unsignedInteger('need_id');
            $table->integer('value')->unsigned();
            $table->integer('decrease_interval');
            $table->timestamp('decrease_updated_at')->nullable();
            $table->timestamp('increase_updated_at')->nullable();
        });

        Schema::table('pet_needs', function (Blueprint $table) {
            $table->foreign('user_pets_id')->references('id')->on('user_pets');
            $table->foreign('need_id')->references('id')->on('needs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pet_needs', function (Blueprint $table) {
            $table->dropForeign('pet_needs_user_pets_id_foreign');
            $table->dropForeign('pet_needs_need_id_foreign');
        });

        Schema::dropIfExists('pet_needs');
    }
}
