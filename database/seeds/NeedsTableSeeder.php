<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Need;

class NeedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('needs')->insert([
            [
                'id' => Need::HUNGER,
                'name' => 'hunger',
                'default_decrease_interval' => Need::HUNGER_DECREASE,
                'default_increase_interval' => Need::HUNGER_INCREASE,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id' => Need::SLEEP,
                'name' => 'sleep',
                'default_decrease_interval' => Need::SLEEP_DECREASE,
                'default_increase_interval' => Need::SLEEP_INCREASE,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id' => Need::CARE,
                'name' => 'care',
                'default_decrease_interval' => Need::CARE_DECREASE,
                'default_increase_interval' => Need::CARE_INCREASE,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ]);
    }
}
