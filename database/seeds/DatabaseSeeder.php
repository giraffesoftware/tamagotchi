<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PetsTableSeeder::class);
        $this->call(NeedsTableSeeder::class);
        //$this->call(TestSeeder::class);
    }
}
