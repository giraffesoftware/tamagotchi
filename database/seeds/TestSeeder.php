<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Need;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'test1',
                'email' => 'test1@test.test',
                'password' => Hash::make('987654321'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'test2',
                'email' => 'test2@test.test',
                'password' => Hash::make('987654321'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ]);
        DB::table('user_pets')->insert([
            [
                'user_id' => 1,
                'pet_id' => 2,
                'alive' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'user_id' => 2,
                'pet_id' => 3,
                'alive' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);
        DB::table('pet_needs')->insert([
            [
                'user_pets_id' => 1,
                'need_id' => Need::HUNGER,
                'value' => 100,
                'decrease_interval' => Need::HUNGER_DECREASE,
                'decrease_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'increase_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'user_pets_id' => 1,
                'need_id' => Need::SLEEP,
                'value' => 100,
                'decrease_interval' => Need::SLEEP_DECREASE,
                'decrease_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'increase_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'user_pets_id' => 1,
                'need_id' => Need::CARE,
                'value' => 100,
                'decrease_interval' => Need::CARE_DECREASE,
                'decrease_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'increase_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'user_pets_id' => 2,
                'need_id' => Need::HUNGER,
                'value' => 100,
                'decrease_interval' => Need::HUNGER_DECREASE,
                'decrease_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'increase_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'user_pets_id' => 2,
                'need_id' => Need::SLEEP,
                'value' => 100,
                'decrease_interval' => Need::SLEEP_DECREASE,
                'decrease_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'increase_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'user_pets_id' => 2,
                'need_id' => Need::CARE,
                'value' => 100,
                'decrease_interval' => Need::CARE_DECREASE,
                'decrease_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'increase_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ]);
    }
}
