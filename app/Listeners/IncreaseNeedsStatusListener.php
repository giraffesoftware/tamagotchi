<?php

namespace App\Listeners;

use App\Events\IncreaseNeedsStatusEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class IncreaseNeedsStatusListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IncreaseNeedsStatusEvent  $event
     * @return void
     */
    public function handle(IncreaseNeedsStatusEvent $event)
    {
        //
    }
}
