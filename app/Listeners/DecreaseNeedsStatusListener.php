<?php

namespace App\Listeners;

use App\Events\DecreaseNeedsStatusEvent;

class DecreaseNeedsStatusListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DecreaseNeedsStatusEvent  $event
     * @return void
     */
    public function handle(DecreaseNeedsStatusEvent $event)
    {
        // Access the order using $event->order...
    }
}