<?php

namespace App\Listeners;

use App\Events\PetDiedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PetDiedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PetDiedEvent  $event
     * @return void
     */
    public function handle(PetDiedEvent $event)
    {
        //
    }
}
