<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Need extends Model
{
    const HUNGER_DECREASE = 10;
    const SLEEP_DECREASE = 20;
    const CARE_DECREASE = 15;

    const HUNGER_INCREASE = 5;
    const SLEEP_INCREASE = 10;
    const CARE_INCREASE = 5;

    const HUNGER = 1;
    const SLEEP = 2;
    const CARE = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'default_decrease_interval', 'default_increase_interval'
    ];
}