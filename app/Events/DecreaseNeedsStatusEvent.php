<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\PetNeed;

class DecreaseNeedsStatusEvent  implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $needId;
    public $value;
    public $userPetId;
    private $userId;

    /**
     * DecreaseNeedsStatusEvent constructor.
     *
     * DecreaseNeedsStatusEvent constructor.
     * @param PetNeed $petNeed
     */
    public function __construct(PetNeed $petNeed)
    {
        $this->needId = $petNeed->need_id;
        $this->value = $petNeed->value;
        $this->userPetId = $petNeed->user_pets_id;
        $this->userId = $petNeed->userPet->user_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('pet-status-'.$this->userId);
    }
}
