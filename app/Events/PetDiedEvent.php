<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\UserPet;

class PetDiedEvent  implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userPetId;
    private $userId;

    /**
     * Create a new event instance.
     *
     * PetDiedEvent constructor.
     * @param UserPet $userPet
     */
    public function __construct(UserPet $userPet)
    {
        $this->userPetId = $userPet->id;
        $this->userId = $userPet->user_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('pet-status-'.$this->userId);
    }
}
