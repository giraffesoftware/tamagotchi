<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    const DOG = 1;
    const CAT = 2;
    const RACCOON = 3;
    const PENGUIN = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}