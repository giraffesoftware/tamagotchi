<?php

namespace App\Policies;

use App\User;
use App\UserPet;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPetPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function isUserPet(User $user, UserPet $userPet)
    {
        return $userPet->user_id == $user->id && $userPet->alive == 1;
    }
}
