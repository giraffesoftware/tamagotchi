<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPet extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'pet_id', 'alive'
    ];

    public function petNeeds()
    {
        return $this->hasMany(PetNeed::class, 'user_pets_id', 'id');
    }
}
