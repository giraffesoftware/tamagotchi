<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetNeed extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_pets_id', 'need_id', 'value', 'interval', 'decrease_updated_at', 'increase_updated_at'
    ];

    public $timestamps = false;

    public function need()
    {
        return $this->belongsTo(Need::class, 'need_id', 'id');
    }

    public function userPet()
    {
        return $this->belongsTo(UserPet::class, 'user_pets_id', 'id');
    }

}