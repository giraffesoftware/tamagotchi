<?php

namespace App\Services;

use App\Need;

class NeedsService
{
    /**
     * return needs
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getNeeds()
    {
        return Need::all();
    }
}