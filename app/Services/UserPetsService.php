<?php

namespace App\Services;

use App\Need;
use App\PetNeed;
use App\UserPet;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserPetsService
{
    public function setPet(int $userId, int $petId)
    {
        return DB::transaction(function () use ($userId, $petId) {
            $userPet = UserPet::create([
                'user_id' => $userId,
                'pet_id' => $petId,
                'alive' => 1
            ]);
            PetNeed::insert([
                [
                    'user_pets_id' => $userPet->id,
                    'need_id' => Need::HUNGER,
                    'value' => 100,
                    'decrease_interval' => Need::HUNGER_DECREASE,
                    'decrease_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'increase_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
                [
                    'user_pets_id' => $userPet->id,
                    'need_id' => Need::SLEEP,
                    'value' => 100,
                    'decrease_interval' => Need::SLEEP_DECREASE,
                    'decrease_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'increase_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ],
                [
                    'user_pets_id' => $userPet->id,
                    'need_id' => Need::CARE,
                    'value' => 100,
                    'decrease_interval' => Need::CARE_DECREASE,
                    'decrease_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'increase_updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]
            ]);
            return $userPet;
        });
    }

    public function getSelectedPets(int $userId)
    {
        return UserPet::where(['user_id' => $userId])->get();
    }

    public function getSelectedPet(int $userPetId)
    {
        return PetNeed::where(['user_pets_id' => $userPetId])->with('userPet')->get();
    }
}