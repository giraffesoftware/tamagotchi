<?php

namespace App\Services;

use App\Pet;

class PetsService
{
    public function getPets()
    {
        return Pet::all();
    }
}