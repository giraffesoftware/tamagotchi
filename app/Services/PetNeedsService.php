<?php

namespace App\Services;

use App\Events\PetDiedEvent;
use App\Need;
use App\PetNeed;
use App\UserPet;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Events\DecreaseNeedsStatusEvent;
use App\Events\IncreaseNeedsStatusEvent;

class PetNeedsService
{
    /**
     * decrease value of pets needs
     */
    public function needsValueDecrease()
    {
        $havingRow = '(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(decrease_updated_at))/60 >= decrease_interval';
        $petNeedsForDecrease = PetNeed::whereRaw($havingRow)->get();
        $petNeedsIds = $this->getIdsFromCollection($petNeedsForDecrease);
        PetNeed::whereIn('id', $petNeedsIds)->where('value', '>', 0)->update([
            'value' => DB::raw('value-1'),
            'decrease_updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        $this->decreaseNeedsStatusBySocket($petNeedsIds);
    }

    /**
     * kill pets
     */
    public function killPets()
    {
        $whereRawCondition = sprintf('(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(decrease_updated_at))/60 >= %d
            and (need_id = %d and value = 0)', env('DIE_INTERVAL'), Need::HUNGER);
        $deadPets = PetNeed::whereRaw($whereRawCondition)->get();
        $deadPetIds = $this->getIdsFromCollection($deadPets, 'user_pets_id');
        UserPet::whereIn('id', $deadPetIds)->where('alive', 1)->update(['alive' => 0]);
        $this->sendPetDiedStatusBySocket($deadPetIds);
    }

    /**
     * change the care decrease interval when sleep need more or less 5 point
     */
    public function adjustCareNeedsInterval()
    {
        $tiredPets = PetNeed::where('need_id', Need::SLEEP)
            ->whereBetween('value', [0,10])->get();
        /**
         * Used interval from 0 to 10 for optimize request. Didn't use interval form 4 to 5 for case
         * of a malfunction in the operation of the cron
         */

        $careStatusOfPets = PetNeed::where([
            ['need_id', '=',  Need::CARE],
        ])->get()->keyBy('user_pets_id');

        $care = Need::where('id', Need::CARE)->firstOrFail();

        $peppyPetIds = [];
        foreach ($tiredPets as $key => $tyredPet) {
            if ($careStatusOfPets[$tyredPet->user_pets_id]->decrease_interval != $care->default_decrease_interval) {
                if ($tyredPet->value < env('CRITICAL_LACK_OF_SLEEP')) {
                    $tiredPets->forget($key);
                } else {
                    $peppyPetIds[] = $tyredPet->user_pets_id;
                    $tiredPets->forget($key);
                }
            } else {
                if ($tyredPet->value >= env('CRITICAL_LACK_OF_SLEEP')) {
                    $tiredPets->forget($key);
                }
            }
        }

        $tiredPetIds = $this->getIdsFromCollection($tiredPets, 'user_pets_id');
        $this->changeCareInterval($tiredPetIds, false);
        $this->changeCareInterval($peppyPetIds);
    }

    /**
     * increase needs point
     *
     * @param int $userPetsId
     * @param int $needsId
     */
    public function increaseNeedsValue(int $userPetsId, int $needsId)
    {
        $petNeedForIncrease = PetNeed::where([
            'user_pets_id' => $userPetsId,
            'need_id' => $needsId
        ])->firstOrFail();

        if ($petNeedForIncrease->value <= 99) {
            $whereRawCondition = sprintf('pet_needs.increase_updated_at + 
            INTERVAL needs.default_increase_interval MINUTE < NOW() 
            AND pet_needs.need_id = %d AND pet_needs.user_pets_id = %d',
                $needsId, $userPetsId);

            $isUpdated = PetNeed::join('needs', 'needs.id', '=', 'pet_needs.need_id')
                ->join('user_pets', 'user_pets.id', '=', 'pet_needs.user_pets_id')
                ->where('user_pets.alive', 1)
                ->whereRaw($whereRawCondition)->update([
                    'pet_needs.value' => DB::raw('pet_needs.value+1'),
                    'pet_needs.increase_updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            $this->increaseNeedsStatusBySocket($petNeedForIncrease->id, $isUpdated);
        }
    }

    /**
     * change the care decrease interval when sleep need more or less 5 point
     *
     * @param array $petIds
     * @param bool $increase
     */
    private function changeCareInterval(array $petIds, bool $increase = true)
    {
        $mathSymbol = $increase ? '*' : '/';
        DB::table('pet_needs')->whereIn('user_pets_id', $petIds)
            ->where('need_id', Need::CARE)
            ->update(['decrease_interval' => DB::raw('decrease_interval' . $mathSymbol . env('INTERVAL_DIVIDER'))]);
    }

    /**
     * @param array $petNeedsIds
     */
    private function decreaseNeedsStatusBySocket(array $petNeedsIds)
    {
        $changedPetsNeeds = PetNeed::whereIn('id', $petNeedsIds)->with('userPet')->get();
        foreach ($changedPetsNeeds as $changedPetsNeed) {
            broadcast(new DecreaseNeedsStatusEvent($changedPetsNeed))->toOthers();
        }
    }

    /**
     * @param int $petNeedsId
     * @param $isUpdated
     */
    private function increaseNeedsStatusBySocket(int $petNeedsId, int $isUpdated)
    {
        $changedPetsNeed = PetNeed::where('id', $petNeedsId)->with('userPet')->firstOrFail();
        broadcast(new IncreaseNeedsStatusEvent($changedPetsNeed, (bool) $isUpdated))->toOthers();
    }

    /**
     * @param array $userPetIds
     */
    private function sendPetDiedStatusBySocket(array $userPetIds)
    {
        $userPets = UserPet::whereIn('id', $userPetIds)->get();
        foreach ($userPets as $userPet) {
            broadcast(new PetDiedEvent($userPet))->toOthers();
        }
    }

    /**
     * @param $collection
     * @param string $keyBy
     * @return array
     */
    private function getIdsFromCollection($collection, $keyBy = 'id')
    {
        return array_keys($collection->keyBy($keyBy)->toArray());
    }
}