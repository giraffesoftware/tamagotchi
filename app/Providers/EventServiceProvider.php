<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\DecreaseNeedsStatusEvent' => [
            'App\Listeners\DecreaseNeedsStatusListener',
        ],
        'App\Events\IncreaseNeedsStatusEvent' => [
            'App\Listeners\IncreaseNeedsStatusListener',
        ],
        'App\Events\PetDiedEvent' => [
            'App\Listeners\PetDiedListener',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
