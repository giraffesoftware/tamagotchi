<?php

namespace App\Http\Controllers;

use App\Services\NeedsService;
use App\Services\PetsService;
use App\Services\UserPetsService;
use App\UserPet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserPetsController extends Controller
{
    /**
     * return page with the pets which user can create
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createPet()
    {
        $userPets = app(UserPetsService::class)->getSelectedPets(Auth::user()->id)->KeyBy('pet_id')->toArray();
        $pets = app(PetsService::class)->getPets()->KeyBy('id')->toArray();
        $pets = array_diff_key($pets, $userPets);
        return view('create_pet', compact('pets'));
    }

    /**
     * return page with the selected users pets
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function selectedPets()
    {
        $userPets = app(UserPetsService::class)->getSelectedPets(Auth::user()->id)->KeyBy('pet_id')->toArray();
        $pets = app(PetsService::class)->getPets()->KeyBy('id')->toArray();
        $pets = array_intersect_key($pets, $userPets);
        foreach ($pets as $key => $pet) {
            $pets[$key]['user_pet_id'] = $userPets[$key]['id'];
            $pets[$key]['alive'] = $userPets[$key]['alive'];
        }
        return view('selected_pets', compact('pets'));
    }

    /**
     * set a new pet for a user
     *
     * @param $pet_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setPet($pet_id)
    {
        $userPet = app(UserPetsService::class)->setPet(Auth::user()->id, $pet_id);
        return redirect()->route('pet', ['user_pet_id' => $userPet->id]);
    }

    /**
     * return pet page
     *
     * @param $user_pet_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPet($user_pet_id)
    {
        if (!Auth::user()->can('isUserPet', UserPet::where('id', $user_pet_id)->firstOrFail())) {
            abort(403);
        }
        return view('pet', compact('user_pet_id'));
    }

    /**
     * return information about pet
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSelectedPetInfo(Request $request)
    {
        if (!Auth::user()->can('isUserPet', UserPet::where('id', $request->input('user_pet_id'))->firstOrFail())) {
            abort(403);
        }
        $petInfo = app(UserPetsService::class)->getSelectedPet($request->input('user_pet_id'));
        $needsList = app(NeedsService::class)->getNeeds();
        $pets = app(PetsService::class)->getPets()->toArray();
        return response()->json([$petInfo, $needsList, $pets]);
    }
}