<?php

namespace App\Http\Controllers;

use App\Http\Requests\IncreaseNeeds;
use App\Services\PetNeedsService;
use App\UserPet;
use Illuminate\Support\Facades\Auth;

class PetNeedsController extends Controller
{
    /**
     * @param IncreaseNeeds $request
     */
    public function increaseNeeds(IncreaseNeeds $request)
    {
        if (!Auth::user()->can('isUserPet', UserPet::where('id', $request->input('user_pet_id'))
            ->firstOrFail())) {
            abort(403);
        }
        app(PetNeedsService::class)->increaseNeedsValue(
            $request->input('user_pet_id'),
            $request->input('need_id')
        );
    }
}
