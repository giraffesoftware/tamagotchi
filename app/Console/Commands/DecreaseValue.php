<?php

namespace App\Console\Commands;

use App\Services\PetNeedsService;
use Illuminate\Console\Command;

class DecreaseValue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:decreaseValue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'decrease value of needs by cron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app(PetNeedsService::class)->needsValueDecrease();
    }
}
