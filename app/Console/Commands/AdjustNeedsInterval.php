<?php

namespace App\Console\Commands;

use App\Services\PetNeedsService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AdjustNeedsInterval extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:adjustNeedsInterval';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change the care decrease interval when sleep need more or less 5 point';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app(PetNeedsService::class)->adjustCareNeedsInterval();
    }
}
