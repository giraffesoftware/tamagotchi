<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::get('/', 'UserPetsController@createPet')->name('create-pet');
    Route::get('selected-pets', 'UserPetsController@selectedPets')->name('selected-pets');
    Route::get('set-pet/{pet_id}', 'UserPetsController@setPet')->name('set-pet');
    Route::get('pet/{user_pet_id}', 'UserPetsController@getPet')->name('pet');
    Route::post('selected-pet-info', 'UserPetsController@getSelectedPetInfo')->name('selected-pet-info');
    Route::post('increase-interval', 'PetNeedsController@increaseNeeds')->name('increase-interval');
});